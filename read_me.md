# Summary
Localized crime data is both relevant and difficult to interpret by normal people at a glance. Most Law Enforcement Agencies partake in a open data policy and have information that is accessible. The goal of this project was to gain an understanding on how this data is organized, constructed, and manipulated. 

# 1. Data

Range of our data
Rows: 23k

Current columns in this Dataset:

* Case Number
    * A unique non repeating ID 
* Case Date Time
    * Month, Day, Hour, Minute
* Case Location
    * Block level address or intesection of the reported crime.
    * Unique String 
* Case Offense Location Type
    * Categorical descriptor
    * 79 Unique 
    * Top Entry: Apartment/Condos
* Case Offense Category
    * Category of the highest crime committed.
    * 13 Unique
    * Top Entry: Theft
* Case Offense Type
    * Type of the highest crime committed or attempted
    * 25 Unique
    * Top Entry: All other larceny
* Case Offense Charge Type
    * Was the crime committed or attempted
    * 3 Unique
    * Top Entry:Committed
* Case Disposition
    * Indication of the status or disposition 
    * 5 Unique
    * Top Entry: Closed
* Status
    * Categorical descriptor
    * 3 Unique
    * Top Entry: Mapped
* Location  
    * Long-Lat coordinates


**Special Note** was provided by the Dataset Owner:

This dataset comes from the Orlando Police Department records management system. It includes all Part 1 and Part 2 crimes as defined by the FBI’s Uniform Crime Reporting standards. When multiple crimes are committed the highest level of crime is what is shown. The data includes only Open and Closed cases and does not include informational cases without arrests. This data excludes crimes where the victim or offender data is or could be legally protected.

Cases that are flagged as:

* Domestic Violence
* Elderly Abuse
* Exempt from Public Record
* Witness Protection
* Cases where the offender was charged with the following case codes/state statutes:
* Sexual Battery
* Child Abuse
* Aggravated Stalking/Harassment
* Obscene or harassing telephone calls
* Aggravated Battery
* Domestic Violence Injunctions
* Lewd,Lascivious or Indecent Assault upon or in the presence of a child
* Elder Abuse
* Baker Act
* Obscenity
* Forcible Rape
* Forcible Sodomy
* Forcible Fondling
* Cases where the a juvenile is arrested

# 2. Method 
## Feature Engineneering 
Primary Features utilized from the dataset where the Crime Category, Crime Type, Academic Season, Date Time Variables, and many features expanded from these topics. 

Creation of Feature Sets
* Set 1 - Isolated Date Time Variables
    * feature_set1 = ['Year','Month','Day','DayLinear','Hour']
* Set 2 - Addition of Longitude and Latitude
    * feature_set2 = ['Year','Month','Day','DayLinear','Hour', 'Longitude', 'Latitude']
* Set 3 - Addition of Academic Season
    * feature_set3 = ['Year','Month','Day','DayLinear','Hour', 'Longitude', 'Latitude', 'Spring', 'Fall', 'Break', 'Summer']
* Set 4 - Addition of Dummy Crime Category
    * feature_set4 = ['Year','Month','Day','DayLinear','Hour', 'Longitude', 'Latitude', 'Spring', 'Fall', 'Break', 'Summer', 'Arson', 'Assault', 'Bribery','Burglary','Embezzlement','Fraud','Homicide','Kidnapping','Narcotics','Robbery', 'Vehicle Theft']

# 3. EDA

A majority of this project was invested into the EDA and getting a better understanding of how the data and features interacted. More information and many visualizations can be found here: https://gitlab.com/dwayne.negron1/capstone-project-aggression-analysis/-/blob/master/CapstoneEDA.ipynb

# 4. Algorithms & Machine Learning 

Primary focus of this project was developing a fundamental understanding of how logistic regressions, random forest, and KNN interacted with the data. 

Results of our modelling can be found here: https://gitlab.com/dwayne.negron1/capstone-project-aggression-analysis/-/blob/master/Capstone2%20Modeling.ipynb 

# 5. Observations

In our modelling we had an interesting interaction where the amount of features added did not uniformly increase performance of all models. For logistic regression and random forest, performance went down with the introduction of additional feature sets (2&3) and skyrocketed with feature set 4. For our KNN model, feature sets 2-4 incrementally increased performance. This led me to believe that something in feature set 4 was causing overfitting in logistic regression and random forest but not KNN.  

# 6. Conclusion

A large challenge of this project was deciding on a correct perspective for addressing the time series. It was easy to organize the data in a way which introduced uneeded seasonality to the data. 

It was very easy to invest a majority of the time in this project into the EDA. The data was interesting and on the surface level appeared to indicate many trends. I believe this was a mistake and should be valued less in future projects. It is important to have a concrete start and finish to the EDA in order to allow for the ML to do its job. 

For this project KNN ended up being the model of choice classifying our target variable with the most dependable performance. 

# 7. Future Improvements 

If I was to do this project again, I would try to start from the beginning again and try to focus on developing a non-classification problem from this dataset and develop a timeseries which can predict the amount of crime at a given time. 

I would be very interested in revisiting this data with an updated dataset and seeing what other features could be pulled. 

Of large interest to me is developing a plotly dashboard for this project to create an interactive tool that could be of use to the Central Florida community. 

