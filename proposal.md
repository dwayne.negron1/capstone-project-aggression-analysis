# Capstone Project Two: Aggression Analysis
## By Dwayne Negron 

### Abstract

Using Data provided by the City of Orlando we would like to create an interative UI via Plotly Dash which analyzes previous trends in crime and predicts future trends via linear regression analysis. Questions of interest include, the effect of COVID-19 and Black Lives Matter movement on local crime compared to previous years, consistent trends which maintain year after year if any, and what are the relationships between **Case Offense Category**, **Location**, and **Case Date Time**.  

### Dataset 

Range of our data
Rows: 23k

Current columns in this Dataset:

* Case Number
	* A unique non repeating ID 
* Case Date Time
	* Month, Day, Hour, Minute
* Case Location
	* Block level address or intesection of the reported crime.
	* Unique String 
* Case Offense Location Type
	* Categorical descriptor
	* 79 Unique 
	* Top Entry: Apartment/Condos
* Case Offense Category
	* Category of the highest crime committed.
	* 13 Unique
	* Top Entry: Theft
* Case Offense Type
	* Type of the highest crime committed or attempted
	* 25 Unique
	* Top Entry: All other larceny
* Case Offense Charge Type
	* Was the crime committed or attempted
	* 3 Unique
	* Top Entry:Committed
* Case Disposition
	* Indication of the status or disposition 
	* 5 Unique
	* Top Entry: Closed
* Status
	* Categorical descriptor
	* 3 Unique
	* Top Entry: Mapped
* Location  
	* Long-Lat coordinates


**Special Note** was provided by the Dataset Owner:

This dataset comes from the Orlando Police Department records management system. It includes all Part 1 and Part 2 crimes as defined by the FBI’s Uniform Crime Reporting standards. When multiple crimes are committed the highest level of crime is what is shown. The data includes only Open and Closed cases and does not include informational cases without arrests. This data excludes crimes where the victim or offender data is or could be legally protected.

Cases that are flagged as:

* Domestic Violence
* Elderly Abuse
* Exempt from Public Record
* Witness Protection
* Cases where the offender was charged with the following case codes/state statutes:
* Sexual Battery
* Child Abuse
* Aggravated Stalking/Harassment
* Obscene or harassing telephone calls
* Aggravated Battery
* Domestic Violence Injunctions
* Lewd,Lascivious or Indecent Assault upon or in the presence of a child
* Elder Abuse
* Baker Act
* Obscenity
* Forcible Rape
* Forcible Sodomy
* Forcible Fondling
* Cases where the a juvenile is arrested