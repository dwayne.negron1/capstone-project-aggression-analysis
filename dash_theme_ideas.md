# Dash Themes Proposal
## By Dwayne Negron

### Capstone 2: Dash Template Ideas
For the Aggression Analysis capstone project we will be providing trend analyst and predictions for categorical data relevant too <Lat, Long>. Rather then create a full GUI from scratch, Dash provides free for use templates that we can then manipulate. 

The target audience for this information would be non-commercial general use. The information provided should help to deliver an understanding of current, past, and potential crime as reported by OPD. The information would be limited to Orlando, Florida. 

The interactive GUI ideally should represent the geospatial area and have categorical modifiers pulled from Crime Type & Crime Category to one side. The GUI should also provide linear regression plots for the interactives with varying sample pools.  

I have listed underneath four potential options for Dash Template. 

### Option A: UBER Data App

[Link](https://dash-gallery.plotly.host/dash-uber-rides-demo/)

The GUI is clean and interactive. It offers a vertical split with a Map element up top, graphplot underneath, and categorical modifiers on the left side. The UI color theme is neutral and the view focused. I think that the template could work but would suffer from multiple graphplot elements. 

### Option B: Rate of US Poison-Induced Deaths

[Link](https://dash-gallery.plotly.host/dash-opioid-epidemic/)

Modern, tech-saavy, GUI. The slider is an effective categorical modifer that provides intuitive date-time manipulation. The color scheme is easy on the eyes and provides a sense of professionalism. The graphplot element is hidden underneath and would be easy to miss. The user interaction with the map element was unintutive but ultimately effective, allowing the user to select an area via lasso tool. Using zip code and providing a proximal area would have been more effective.  

### Option C: DASH Clinical Analytics

[Link](https://dash-gallery.plotly.host/dash-medical-provider-charges/)

The GUI provides over five categorical modifiers that lends itself to an extremely busy feel. I find the color theme to be unintuitive. The map element effectively portrays the information and displays it in a way similiar to what we intend (radial scatterplot). I think overall the GUI is ineffective and its graphplot the weakest element. The biggest advantage I would see of Option C, is how it provides categorical elements in a top down direction. So as you fill out modifiers you are developing a narrative for the map element.  

### Option D: New York Oil and Gas

[Link](https://dash-gallery.plotly.host/dash-oil-and-gas/)

The GUI utilizes material design and is very bright. The cards function to isolate key variables and elements. I think the template could be effective but ultimately balances on the knife edge between convoluted and informational. The categorical modiferes on the left side of the template are an effective means of interacting with the map element. 