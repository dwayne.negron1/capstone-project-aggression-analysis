import pandas as pd

# This is an exploratory analysis and wrangling of the OPD data for the Aggression Analysis project.

df = pd.read_csv("OPD_Crimes.csv")
print(df.head())
print(df.describe())

# Does our dataset have any duplicates?
df_dup = df.duplicated(subset=['Case Number'])
print("How many duplicates are found in the Case Number column? ", df_dup.sum())
# We have one duplicate within our Dataset potentially

# Missing Values?
print("How many Missing Values are found in the Case Number column? ", df['Case Number'].isnull().sum())
# None
print()

# Describing the Columns in question
print(df['Case Offense Location Type'].describe())
print(df['Case Offense Category'].describe())
print(df['Case Offense Type'].describe())
print(df['Case Offense Charge Type'].describe())
print(df['Case Disposition'].describe())
print(df['Status'].describe())
print(df['Location'].describe())

#print("date max",df_date_max,"date min",df_date_min)

# What are the Offense Categories and How Many?
df_off_cat = df.groupby("Case Offense Category").count()
print(df_off_cat['Case Number'],'\n')

# What are the Offense Types and How Many?
df_off_type = df.groupby("Case Offense Type").count()
print(df_off_type['Case Number'],'\n')

# Will Case Disposition be useful in our analysis?
df_dispo = df.groupby("Case Disposition").count()
print(df_dispo['Case Number'])
# I do not believe it will lend itself well to our analysis

