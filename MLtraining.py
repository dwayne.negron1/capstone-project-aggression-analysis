import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import seaborn as sns
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix

# Local DF loaded
df = pd.read_csv("OPD_Crimes.csv", parse_dates = ['Case Date Time'])
df = df.drop(columns = ['Location'])
df = df.dropna()

# Addition of dt elements parsed from Case Date Time using pd.
df['Date'] = df['Case Date Time'].dt.date
df['Year'] = df['Case Date Time'].dt.year
df['Month'] = df['Case Date Time'].dt.month
df['Day'] = df['Case Date Time'].dt.dayofweek
df['DayLinear']= df['Case Date Time'].dt.day
df['Hour'] = df['Case Date Time'].dt.hour

df['Theft'] = df['Case Offense Category'] == 'Theft'

feature_cols = ['Year','Day', 'Month', 'Hour', 'DayLinear']
X = df[feature_cols]

y = df['Theft']

from sklearn import preprocessing
le = preprocessing.LabelEncoder()
y = le.fit_transform(y)

from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25,random_state=0)
logreg = LogisticRegression(max_iter = 30000)
logreg.fit(X_train,y_train)
y_pred = logreg.predict(X_test)
# correct_pred = y_pred == y_test
# logreg.score(X_train, y_train)

# import the metrics class
from sklearn import metrics
cnf_matrix = metrics.confusion_matrix(y_test, y_pred)
print(cnf_matrix)

# import required modules
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


class_names=[0,1] # name  of classes
fig, ax = plt.subplots()
tick_marks = np.arange(len(class_names))
plt.xticks(tick_marks, class_names)
plt.yticks(tick_marks, class_names)
# create heatmap
sns.heatmap(pd.DataFrame(cnf_matrix), annot=True, cmap="YlGnBu" ,fmt='g')
ax.xaxis.set_label_position("top")
plt.tight_layout()
plt.title('Confusion matrix for Theft', y=1.1)
plt.ylabel('Actual label')
plt.xlabel('Predicted label')

plt.show()

print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
print("Precision:",metrics.precision_score(y_test, y_pred))
print("Recall:",metrics.recall_score(y_test, y_pred))

y_pred_proba = logreg.predict_proba(X_test)[::,1]
fpr, tpr, _ = metrics.roc_curve(y_test,  y_pred_proba)
auc = metrics.roc_auc_score(y_test, y_pred_proba)
plt.plot(fpr,tpr,label="data 1, auc="+str(auc))
plt.title('Theft AUC')
plt.legend(loc=4)
plt.show()

#logreg.score(X_test, y_test)

#print(correct_pred.count())
#model = LogisticRegression(max_iter=3000)
#model.fit(X,y)
#y_pred = model.predict(X,y)
#correct_pred = y_pred == y
#print(correct_pred.sum())
#model.score(X,y)

#dfT = df[df['Case Offense Category'] == 'Theft']
#dfT = dfT[['Case Number', 'Case Offense Type', 'Year', 'Month', 'Day', 'DayLinear', 'Hour']]
#dfT = pd.concat([dfT,dfY])
#print(dfT.columns)
#print(dfY.head())
#dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']


#y = dfT['Case Offense Type']
#x = dfT.drop('Case Offense Type', axis=1)

#print(y.head())
#print(y.describe())
#print(x.head())
#print(x.describe())

#x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.2)
#x_train.head()
#x_train.shape
#x_test.head()
#x_test.shape


#corrs = dfY.corr().abs()
#columns = corrs[corrs > .1].index
#corrs = corrs.filter(columns)
#print(corrs)
#y=data.temp
#>>> x=data.drop('temp',axis=1)
