import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import seaborn as sns
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix

# Local DF loaded
df = pd.read_csv("OPD_Crimes.csv", parse_dates = ['Case Date Time'])
df = df.drop(columns = ['Location'])
df = df.dropna()

# Addition of dt elements parsed from Case Date Time using pd.
df['Date'] = df['Case Date Time'].dt.date
df['Year'] = df['Case Date Time'].dt.year
df['Month'] = df['Case Date Time'].dt.month
df['Day'] = df['Case Date Time'].dt.dayofweek
df['DayLinear']= df['Case Date Time'].dt.day
df['Hour'] = df['Case Date Time'].dt.hour

# Quick EDA
print(df['Case Offense Type'].value_counts())
print(df['Case Offense Category'].value_counts())

#Feature Call
df['Theft'] = df['Case Offense Category'] == 'Theft'
df['Larceny'] = df['Case Offense Type'] == 'All other larceny'
#df['Category Dummies'] = pd.get_dummies(df, columns = ['Case Offense Category'])
features = ['Date','Year','Month','Day','DayLinear','Hour','Theft','Larceny']

corrmat = df[features].corr()
f, ax = plt.subplots(figsize=(12, 9))
hm = sns.heatmap(round(corrmat,2), annot=True, ax=ax, cmap="coolwarm",fmt='.2f',
                 linewidths=.05)

plt.show()
