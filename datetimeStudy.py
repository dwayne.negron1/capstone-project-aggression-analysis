import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import seaborn as sns
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
from patsy.builtins import *

# Local DF loaded
df = pd.read_csv("OPD_Crimes.csv", parse_dates = ['Case Date Time'])

# Addition of dt elements parsed from Case Date Time using pd.
df['Date'] = df['Case Date Time'].dt.date
df['Year'] = df['Case Date Time'].dt.year
df['Month'] = df['Case Date Time'].dt.month
df['Day'] = df['Case Date Time'].dt.dayofweek
df['DayLinear']= df['Case Date Time'].dt.day
df['Hour'] = df['Case Date Time'].dt.hour

# Temporary interactive text based gui for cleaner graph calling and generation

def intro():
    print('''Which of the following visualizations would you like to see?
        1. Total Cases
        2. Case Offense Category
        3. Case Offense Type
        4. Case Offense Location
        5. Case Disposition
        6. Case Charge Type
        7. Theft Month, Year, Type Distribution
        8. Assault Month, Year, Type Distribution
        9. Hobson EXP
    
    Please insert one of the following options 1 2 3 4 5 6 7 or 8 with no spacing
    
    Visualization will generate after a few moments (15-45 seconds)
    
    To exit the program enter the letter e
    
    Close visualizations for the menu to restart
    
    ''')

    choice = input()

    if choice == '1':
        totalCases()
        reroute()
    elif choice == '2':
        caseOffenseCat()
        reroute()
    elif choice == '3':
        caseOffenseType()
        reroute()
    elif choice == '4':
        caseOffenseLoc()
        reroute()
    elif choice == '5':
        caseDisposition()
        reroute()
    elif choice == '6':
        caseChargeType()
        reroute()
    elif choice == '7':
        theftTypeTime()
        reroute()
    elif choice == '8':
        assaultTypeTime()
        reroute()
    elif choice == 'e':
        exit()
    else:
        print("I'm sorry can you please re-enter your choice \n")
        intro()

# Cycling of text based gui for multiple interactions.
def reroute():
    plt.show()
    print('''Would you like to see more visualizations? 
    
        y / n
    ''')
    rr = input()
    if rr != 'n':
        intro()
    else:
        exit()


# Call in for graphing of Case Report sum versus dt variables.
def totalCases():
    df_year = df[['Year', 'Case Date Time']].groupby('Year').count().reset_index()
    df_date = df[['Date', 'Case Date Time']].groupby('Date').count().reset_index()
    df_month = df[['Month', 'Case Date Time']].groupby('Month').count().reset_index()
    df_day = df[['Day', 'Case Date Time']].groupby('Day').count().reset_index()

    fig, ax = plt.subplots(3,2, sharey=True)

    fig.suptitle('Total Crimes vs Year / Month / Day')

    gY = sns.barplot(x='Year', y='Case Date Time', data=df_year, ax=ax[0,0])
    gM = sns.barplot(x='Month', y='Case Date Time', data=df_month, ax=ax[1,0])
    gD = sns.barplot(x='Day', y='Case Date Time', data=df_day, ax=ax[2,0])
    gYr = sns.regplot(x='Year', y='Case Date Time', data=df_year, ax=ax[0,1])
    gMr = sns.regplot(x='Month', y='Case Date Time', data=df_month, ax=ax[1,1])
    gDr = sns.regplot(x='Day', y='Case Date Time', data=df_day, ax=ax[2,1])

    # Graphed plot versus all days in attempt to create a stock market style visualization of crime sum.
    fig, ax= plt.subplots()
    fig.suptitle('Total Crimes Time Sequence')
    g = sns.barplot(x='Date', y='Case Date Time', data=df_date, )
    g.tick_params(bottom=False)
    g.set(xticklabels=[])
    g.set(xlabel = '2010-2019')

# Call in for graphing of Case Offense Categories versus dt variables.
def caseOffenseCat():
    df_year_cat = df[['Year','Case Offense Category','Case Date Time']].groupby(['Year', 'Case Offense Category']).count().reset_index()
    df_month_cat = df[['Month','Case Offense Category','Case Date Time']].groupby(['Month', 'Case Offense Category']).count().reset_index()
    df_day_cat = df[['Day','Case Offense Category','Case Date Time']].groupby(['Day', 'Case Offense Category']).count().reset_index()
    df_day_daily_cat = df[['DayLinear','Case Offense Category','Case Date Time']].groupby(['DayLinear', 'Case Offense Category']).count().reset_index()
    df_hour_cat = df[['Hour','Case Offense Category','Case Date Time']].groupby(['Hour', 'Case Offense Category']).count().reset_index()

    gY = sns.lmplot(x='Year',y='Case Date Time',hue = 'Case Offense Category', data=df_year_cat)
    gM = sns.lmplot(x='Month',y='Case Date Time',hue = 'Case Offense Category', data=df_month_cat)
    gD = sns.lmplot(x='Day',y='Case Date Time',hue = 'Case Offense Category', data=df_day_cat)
    gDl = sns.lmplot(x='DayLinear',y='Case Date Time',hue = 'Case Offense Category', data=df_day_daily_cat)
    gH = sns.lmplot(x='Hour',y='Case Date Time',hue = 'Case Offense Category', data=df_hour_cat)

    # Y axis Label Correction
    gY.set(ylabel='Crime Total')
    gM.set(ylabel='Crime Total')
    gD.set(ylabel='Crime Total')
    gDl.set(ylabel='Crime Total')
    gH.set(ylabel='Crime Total')

# Call in for graphing of Case Offense Type Categories versus dt variables.
def caseOffenseType():
    df_year_type = df[['Year','Case Offense Type','Case Date Time']].groupby(['Year', 'Case Offense Type']).count().reset_index()
    df_month_type = df[['Month','Case Offense Type','Case Date Time']].groupby(['Month', 'Case Offense Type']).count().reset_index()
    df_day_type = df[['Day','Case Offense Type','Case Date Time']].groupby(['Day', 'Case Offense Type']).count().reset_index()
    df_day_daily_type = df[['DayLinear','Case Offense Type','Case Date Time']].groupby(['DayLinear', 'Case Offense Type']).count().reset_index()
    df_hour_type = df[['Hour','Case Offense Type','Case Date Time']].groupby(['Hour', 'Case Offense Type']).count().reset_index()

    gY = sns.lmplot(x='Year',y='Case Date Time',hue = 'Case Offense Type', data=df_year_type)
    gM = sns.lmplot(x='Month',y='Case Date Time',hue = 'Case Offense Type', data=df_month_type)
    gD = sns.lmplot(x='Day',y='Case Date Time',hue = 'Case Offense Type', data=df_day_type)
    gDl = sns.lmplot(x='DayLinear',y='Case Date Time',hue = 'Case Offense Type', data=df_day_daily_type)
    gH = sns.lmplot(x='Hour',y='Case Date Time',hue = 'Case Offense Type', data=df_hour_type)

    # Y axis Label Correction
    gY.set(ylabel='Crime Total')
    gM.set(ylabel='Crime Total')
    gD.set(ylabel='Crime Total')
    gDl.set(ylabel='Crime Total')
    gH.set(ylabel='Crime Total')

# Call in for Case Offense Location Types versus dt variables.
def caseOffenseLoc():
    df_year_loc = df[['Year','Case Offense Location Type','Case Date Time']].groupby(['Year', 'Case Offense Location Type']).count().reset_index()
    df_month_loc = df[['Month','Case Offense Location Type','Case Date Time']].groupby(['Month', 'Case Offense Location Type']).count().reset_index()
    df_day_loc = df[['Day','Case Offense Location Type','Case Date Time']].groupby(['Day', 'Case Offense Location Type']).count().reset_index()
    df_day_daily_loc = df[['DayLinear','Case Offense Location Type','Case Date Time']].groupby(['DayLinear', 'Case Offense Location Type']).count().reset_index()
    df_hour_loc = df[['Hour','Case Offense Location Type','Case Date Time']].groupby(['Hour', 'Case Offense Location Type']).count().reset_index()

    gY = sns.lmplot(x='Year',y='Case Date Time',hue = 'Case Offense Location Type', data=df_year_loc)
    gM = sns.lmplot(x='Month',y='Case Date Time',hue = 'Case Offense Location Type', data=df_month_loc)
    gD = sns.lmplot(x='Day',y='Case Date Time',hue = 'Case Offense Location Type', data=df_day_loc)
    gDl = sns.lmplot(x='DayLinear',y='Case Date Time',hue = 'Case Offense Location Type', data=df_day_daily_loc)
    gH = sns.lmplot(x='Hour',y='Case Date Time',hue = 'Case Offense Location Type', data=df_hour_loc)

    # Y axis Label Correction
    gY.set(ylabel='Crime Total')
    gM.set(ylabel='Crime Total')
    gD.set(ylabel='Crime Total')
    gDl.set(ylabel='Crime Total')
    gH.set(ylabel='Crime Total')

# Call in for Case Disposition Types versus dt variables.
def caseDisposition():
    df_year_disp = df[['Year','Case Disposition','Case Date Time']].groupby(['Year', 'Case Disposition']).count().reset_index()
    df_month_disp = df[['Month','Case Disposition','Case Date Time']].groupby(['Month', 'Case Disposition']).count().reset_index()
    df_day_disp = df[['Day','Case Disposition','Case Date Time']].groupby(['Day', 'Case Disposition']).count().reset_index()
    df_day_daily_disp = df[['DayLinear','Case Disposition','Case Date Time']].groupby(['DayLinear', 'Case Disposition']).count().reset_index()
    df_hour_disp = df[['Hour','Case Disposition','Case Date Time']].groupby(['Hour', 'Case Disposition']).count().reset_index()

    gY = sns.lmplot(x='Year',y='Case Date Time',hue = 'Case Disposition', data=df_year_disp)
    gM = sns.lmplot(x='Month',y='Case Date Time',hue = 'Case Disposition', data=df_month_disp)
    gD = sns.lmplot(x='Day',y='Case Date Time',hue = 'Case Disposition', data=df_day_disp)
    gDl = sns.lmplot(x='DayLinear',y='Case Date Time',hue = 'Case Disposition', data=df_day_daily_disp)
    gH = sns.lmplot(x='Hour',y='Case Date Time',hue = 'Case Disposition', data=df_hour_disp)

    # Y axis Label Correction
    gY.set(ylabel='Crime Total')
    gM.set(ylabel='Crime Total')
    gD.set(ylabel='Crime Total')
    gDl.set(ylabel='Crime Total')
    gH.set(ylabel='Crime Total')

# Call in for Case Charge Types versus dt variables.
def caseChargeType():
    df_year_cType = df[['Year', 'Case Offense Charge Type', 'Case Date Time']].groupby(['Year', 'Case Offense Charge Type']).count().reset_index()
    df_month_cType = df[['Month', 'Case Offense Charge Type', 'Case Date Time']].groupby(['Month', 'Case Offense Charge Type']).count().reset_index()
    df_day_cType = df[['Day', 'Case Offense Charge Type', 'Case Date Time']].groupby(['Day', 'Case Offense Charge Type']).count().reset_index()
    df_day_daily_cType = df[['DayLinear', 'Case Offense Charge Type', 'Case Date Time']].groupby(['DayLinear', 'Case Offense Charge Type']).count().reset_index()
    df_hour_cType = df[['Hour', 'Case Offense Charge Type', 'Case Date Time']].groupby(['Hour', 'Case Offense Charge Type']).count().reset_index()

    gY = sns.lmplot(x='Year', y='Case Date Time', hue='Case Offense Charge Type', data=df_year_cType)
    gM = sns.lmplot(x='Month', y='Case Date Time', hue='Case Offense Charge Type', data=df_month_cType)
    gD = sns.lmplot(x='Day', y='Case Date Time', hue='Case Offense Charge Type', data=df_day_cType)
    gDl = sns.lmplot(x='DayLinear', y='Case Date Time', hue='Case Offense Charge Type', data=df_day_daily_cType)
    gH = sns.lmplot(x='Hour', y='Case Date Time', hue='Case Offense Charge Type', data=df_hour_cType)

    # Y axis Label Correction
    gY.set(ylabel='Crime Total')
    gM.set(ylabel='Crime Total')
    gD.set(ylabel='Crime Total')
    gDl.set(ylabel='Crime Total')
    gH.set(ylabel='Crime Total')

#  Call in Theft Types versus dt variables.
def theftTypeTime():
    dfT = df[df['Case Offense Category'] == 'Theft']
    df_year_TType = dfT[['Year', 'Case Offense Type', 'Case Date Time']].groupby(['Year', 'Case Offense Type']).count().reset_index()
    df_month_TType = dfT[['Month', 'Case Offense Type', 'Case Date Time']].groupby(['Month', 'Case Offense Type']).count().reset_index()
    df_day_TType = dfT[['Day', 'Case Offense Type', 'Case Date Time']].groupby(['Day', 'Case Offense Type']).count().reset_index()
    df_day_daily_TType = dfT[['DayLinear', 'Case Offense Type', 'Case Date Time']].groupby(['DayLinear', 'Case Offense Type']).count().reset_index()
    df_hour_TType = dfT[['Hour', 'Case Offense Type', 'Case Date Time']].groupby(['Hour', 'Case Offense Type']).count().reset_index()

    gY = sns.lmplot(x='Year', y='Case Date Time', hue='Case Offense Type', data=df_year_TType)
    gM = sns.lmplot(x='Month', y='Case Date Time', hue='Case Offense Type', data=df_month_TType)
    gD = sns.lmplot(x='Day', y='Case Date Time', hue='Case Offense Type', data=df_day_TType)
    gDl = sns.lmplot(x='DayLinear', y='Case Date Time', hue='Case Offense Type', data=df_day_daily_TType)
    gH = sns.lmplot(x='Hour', y='Case Date Time', hue='Case Offense Type', data=df_hour_TType)

    # An attempt to plot localized groupings of Theft Types binned by month and plotted via year.
    df_yrmo_TType = dfT[['Year', 'Month', 'Case Offense Type', 'Case Date Time']].groupby(['Year', 'Month', 'Case Offense Type']).count().reset_index()
    fig, ax = plt.subplots()
    g = sns.violinplot(x="Year", y="Case Date Time", hue='Month', data=df_yrmo_TType)
    fig.suptitle('Thefts Over the Years by Month')

    # Y axis Label Correction
    gY.set(ylabel='Theft Total')
    gM.set(ylabel='Theft Total')
    gD.set(ylabel='Theft Total')
    gDl.set(ylabel='Theft Total')
    gH.set(ylabel='Theft Total')
    g.set(ylabel='Theft Total')

#  Call in Assault Types versus dt variables.
def assaultTypeTime():
    dfAggAs = df[df['Case Offense Category'] == 'Assault']
    df_year_AAType = dfAggAs[['Year', 'Case Offense Type', 'Case Date Time']].groupby(['Year', 'Case Offense Type']).count().reset_index()
    df_month_AAType = dfAggAs[['Month', 'Case Offense Type', 'Case Date Time']].groupby(['Month', 'Case Offense Type']).count().reset_index()
    df_day_AAType = dfAggAs[['Day', 'Case Offense Type', 'Case Date Time']].groupby(['Day', 'Case Offense Type']).count().reset_index()
    df_day_daily_AAType = dfAggAs[['DayLinear', 'Case Offense Type', 'Case Date Time']].groupby(['DayLinear', 'Case Offense Type']).count().reset_index()
    df_hour_AAType = dfAggAs[['Hour', 'Case Offense Type', 'Case Date Time']].groupby(['Hour', 'Case Offense Type']).count().reset_index()

    gY = sns.lmplot(x='Year', y='Case Date Time', hue='Case Offense Type', data=df_year_AAType)
    gM = sns.lmplot(x='Month', y='Case Date Time', hue='Case Offense Type', data=df_month_AAType)
    gD = sns.lmplot(x='Day', y='Case Date Time', hue='Case Offense Type', data=df_day_AAType)
    gDl = sns.lmplot(x='DayLinear', y='Case Date Time', hue='Case Offense Type', data=df_day_daily_AAType)
    gH = sns.lmplot(x='Hour', y='Case Date Time', hue='Case Offense Type', data=df_hour_AAType)

    # An attempt to plot localized groupings of Assault Types binned by month and plotted via year.
    df_yrmo_AAType = dfAggAs[['Year', 'Month', 'Case Offense Type', 'Case Date Time']].groupby(['Year', 'Month', 'Case Offense Type']).count().reset_index()
    fig, ax = plt.subplots()
    g = sns.violinplot(x="Year", y="Case Date Time", hue='Month', data=df_yrmo_AAType)
    fig.suptitle('Assaults Over the Years by Month')

    # Y axis Label Correction
    gY.set(ylabel='Assault Total')
    gM.set(ylabel='Assault Total')
    gD.set(ylabel='Assault Total')
    gDl.set(ylabel='Assault Total')
    gH.set(ylabel='Assault Total')
    g.set(ylabel='Assault Total')

def Larc():
    dfT = df[df['Case Offense Category'] == 'Theft']
    dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']
    print('Theft Category\n',dfT.describe())
    print('All Other Larceny Type \n',dfTlarc.describe())


    df_d_hour_Tlarc = dfTlarc[['Hour', 'Case Number']].groupby(['Hour']).count().reset_index()
    df_d_month_Tlarc = dfTlarc[['Month', 'Case Number']].groupby(['Month']).count().reset_index()
    df_d_year_Tlarc = dfTlarc[['Year', 'Case Number']].groupby(['Year']).count().reset_index()
    df_d_day_Tlarc = dfTlarc[['Day', 'Case Number']].groupby(['Day']).count().reset_index()
    df_d_daylinear_Tlarc = dfTlarc[['DayLinear', 'Case Number']].groupby(['DayLinear']).count().reset_index()

    gY = sns.lmplot(x='Year', y='Case Number', data=df_d_year_Tlarc)
    gM = sns.lmplot(x='Month', y='Case Number', data=df_d_month_Tlarc)
    gD = sns.lmplot(x='Day', y='Case Number', data=df_d_day_Tlarc)
    gDl = sns.lmplot(x='DayLinear', y='Case Number', data=df_d_daylinear_Tlarc)
    gH = sns.lmplot(x='Hour', y='Case Number', data=df_d_hour_Tlarc)

    gY.set(ylabel='Larceny Total')
    gM.set(ylabel='Larceny Total')
    gD.set(ylabel='Larceny Total')
    gDl.set(ylabel='Larceny Total')
    gH.set(ylabel='Larceny Total')

    plt.show()

def deltaMeanLarc():
    dfT = df[df['Case Offense Category'] == 'Theft']
    dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']
    print('Theft Category\n',dfT.describe())
    print('All Other Larceny Type \n',dfTlarc.describe())

    dfTlarc['Year Mean'] = dfTlarc['Year'].mean()
    dfTlarc['Month Mean'] = dfTlarc['Month'].mean()
    dfTlarc['Day Mean'] = dfTlarc['Day'].mean()
    dfTlarc['DayLinear Mean'] = dfTlarc['DayLinear'].mean()
    dfTlarc['Hour Mean'] = dfTlarc['Hour'].mean()
    dfTlarc['d Year'] = abs(dfTlarc['Year'] - dfTlarc['Year Mean'])
    dfTlarc['d Month'] = abs(dfTlarc['Month'] - dfTlarc['Month Mean'])
    dfTlarc['d Day'] = abs(dfTlarc['Day'] - dfTlarc['Day Mean'])
    dfTlarc['d DayLinear'] = abs(dfTlarc['DayLinear'] - dfTlarc['DayLinear Mean'])
    dfTlarc['d Hour'] = abs(dfTlarc['Hour'] - dfTlarc['Hour Mean'])

    df_d_hour_Tlarc = dfTlarc[['d Hour', 'Case Number']].groupby(['d Hour']).count().reset_index()
    df_d_month_Tlarc = dfTlarc[['d Month', 'Case Number']].groupby(['d Month']).count().reset_index()
    df_d_year_Tlarc = dfTlarc[['d Year', 'Case Number']].groupby(['d Year']).count().reset_index()
    df_d_day_Tlarc = dfTlarc[['d Day', 'Case Number']].groupby(['d Day']).count().reset_index()
    df_d_daylinear_Tlarc = dfTlarc[['d DayLinear', 'Case Number']].groupby(['d DayLinear']).count().reset_index()

    gY = sns.lmplot(x='d Year', y='Case Number', data=df_d_year_Tlarc)
    gM = sns.lmplot(x='d Month', y='Case Number', data=df_d_month_Tlarc)
    gD = sns.lmplot(x='d Day', y='Case Number', data=df_d_day_Tlarc)
    gDl = sns.lmplot(x='d DayLinear', y='Case Number', data=df_d_daylinear_Tlarc)
    gH = sns.lmplot(x='d Hour', y='Case Number', data=df_d_hour_Tlarc)

    gY.set(ylabel='Larceny Total')
    gM.set(ylabel='Larceny Total')
    gD.set(ylabel='Larceny Total')
    gDl.set(ylabel='Larceny Total')
    gH.set(ylabel='Larceny Total')

    plt.show()

# Needs help
def getDummies():
    dfT = df[df['Case Offense Category'] == 'Theft']
    dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']

    print('Pre Dummies\n', dfT.describe())
    d = pd.get_dummies(dfT[dfT['Case Offense Type'] == 'All other larceny'])
    print(d.describe())

def monthVsMonth():
    dfT = df[df['Case Offense Category'] == 'Theft']
    dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']
    df_hour_Tlarc = dfTlarc[['Hour','Month','Case Number']].groupby(['Hour','Month']).count().reset_index()

    for i in range(12):
        df_hour_Tlarci = df_hour_Tlarc[df_hour_Tlarc['Month'] == (i+1)]
        df_hour_TlarcO = df_hour_Tlarc[df_hour_Tlarc['Month'] != (i+1)]
        gA = sns.barplot(x="Hour", y="Case Number", data=df_hour_Tlarci, alpha=0.3, color='blue')
        gB = sns.barplot(x="Hour", y="Case Number", data=df_hour_TlarcO, alpha=0.3, color='red')
        gA.set(ylabel='Larceny Total')
        gA.set(xlabel='Hour of Day')
        plt.title('Month %i versus All' %(i+1))
        plt.legend(labels=['Blue = Month', 'Red = Others'])
        plt.show()

def deltaMonthvsMonth():
    dfT = df[df['Case Offense Category'] == 'Theft']
    dfTlarc = dfT[dfT['Case Offense Type'] == 'All other larceny']
    dfTlarc['Hour Mean'] = dfTlarc['Hour'].mean()
    dfTlarc['d Hour'] = abs(dfTlarc['Hour'] - dfTlarc['Hour Mean'])
    dfTlarc['d Hour'] = round(((dfTlarc['d Hour'] * 2)+.5))/2
    df_hour_Tlarc = dfTlarc[['d Hour', 'Month', 'Case Number']].groupby(['d Hour', 'Month']).count().reset_index()

    for i in range(12):
        df_hour_Tlarci = df_hour_Tlarc[df_hour_Tlarc['Month'] == (i + 1)]
        df_hour_TlarcO = df_hour_Tlarc[df_hour_Tlarc['Month'] != (i + 1)]
        gA = sns.barplot(x="d Hour", y="Case Number", data=df_hour_Tlarci, alpha=0.3, color='blue')
        gB = sns.barplot(x="d Hour", y="Case Number", data=df_hour_TlarcO, alpha=0.3, color='red')
        gA.set(ylabel='Larceny Total')
        gA.set(xlabel='Distance from Crime Occurence Mean')
        plt.title('Month %i versus All' % (i + 1))
        plt.legend(labels=['Blue = Month', 'Red = Others'])
        plt.show()

#df['Theft'] = df['Case Offense Category'] == 'Theft'
#df['Theft'] = df['Theft'].astype(int)


#print(df['Theft'].head())
#m = ols("Theft ~ Day",df).fit()
#print(m.summary())
#intro()